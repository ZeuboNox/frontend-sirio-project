import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUp } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";


const ButtonUp = () => {
  const [show, setShow] = useState(false);
  const goUp = () => {
    if (show && typeof document !== "undefined") {
      document.querySelector("body").scrollIntoView({
        behavior: "smooth"
      });
    }
  };

  const onScroll = () => {
    if (typeof window === "undefined") return;

    const scrollPosition =
      window.scrollY ||
      window.pageYOffset ||
      document.body.scrollTop +
        ((document.documentElement && document.documentElement.scrollTop) || 0);

    if (scrollPosition > 100) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    if (typeof window === "undefined") return false;

    window.addEventListener("scroll", onScroll, true);
    return () => window.removeEventListener("scroll", onScroll, true);
  }, []);

  return (
    <div className={classNames("ButtonUp", { "is-show": show })} onClick={goUp}>
      <FontAwesomeIcon className="ButtonUp__icon" icon={faArrowUp} />
    </div>
  );
};

export default ButtonUp
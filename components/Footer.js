import React from 'react'
import Link from 'next/link'

function Footer() {
    return(
      <footer className="bg-dark text-center text-white"
        style={{
          margin: '40px 0 0 0',
          bottom:'0',
        }}>
        <div className="container p-4 pb-0">
          <section className="">
            <form action="">
              <div className="row d-flex justify-content-center">
                <div className="col-auto">
                  <p className="pt-2">
                    <strong>S'inscrire pour recevoir les Newsletters</strong>
                  </p>
                </div>

                <div className="col-md-5 col-12">
                  <div className="form-outline form-white mb-4">
                    <input type="email" 
                    id="form5Example2" 
                    className="form-control" 
                    placeholder="Addresse Email"/>
                  </div>
                </div>

                <div className="col-auto">
                  <button type="submit" className="btn btn-outline-light mb-4 text-uppercase">
                    S'inscrire
                  </button>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div className="text-center p-3" 
          style={{background: 'rgba(0, 0, 0, 0.2)',}}>
          © 2021 Copyright
          
        </div>
      </footer>
    )
}

export default Footer
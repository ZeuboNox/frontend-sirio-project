import React from 'react'
import NavBar from './NavBar'
import Notify from './Notify'
import Modal from './Modal'
import ButtonUp from './ButtonUp'
import Footer from './Footer'


function Layout({children}) {
    return (
        <div className="container">
            <NavBar />
            <ButtonUp />
            <Notify />
            <Modal />
            {children}
            <Footer />
        </div>
    )
}

export default Layout
